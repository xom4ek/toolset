#!/bin/bash
gcloud auth login $GLOUD_USERNAME --cred-file=$GCLOUD_CRED_FILE
curl -ivk ${JENKINS_BINARY} -o slave.jar && java ${JAVA_OPTS} -jar slave.jar -jnlpUrl ${JENKINS_SLAVE} -secret ${JENKINS_TOKEN} -workDir "/home/jenkins"
