FROM registry.docker.com/library/python:3.11.4
ENV SOPS_VERSION=3.7.3
RUN apt-get update && apt-get install -y --no-install-recommends git && rm -f /var/cache/apt/archives/*.deb /var/cache/apt/archives/partial/*.deb /var/cache/apt/*.bin || true
RUN pip install --no-cache-dir pre-commit==3.3.3
ENV terraform_version=1.5.6
RUN wget --quiet https://hashicorp-releases.yandexcloud.net/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip \
    && unzip terraform_${terraform_version}_linux_amd64.zip \
    && mv terraform /usr/bin \
    && rm terraform_${terraform_version}_linux_amd64.zip

RUN curl -s https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash
RUN wget -qO /usr/local/bin/sops https://github.com/mozilla/sops/releases/download/v${SOPS_VERSION}/sops-v${SOPS_VERSION}.linux

COPY .ci/pre-commit-config.yaml test/pre-commit-config.yaml
RUN cd test && git init . && pre-commit install -c pre-commit-config.yaml
