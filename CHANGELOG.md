## [2.0.0](https://gitlab.com/xom4ek/toolset/compare/1.2.0...2.0.0) (2022-06-27)


### ⚠ BREAKING CHANGES

* **jenkinsagents:** add kubectl and fix balena install release

### 🚀 Features

* **jenkinsagents:** add kubectl and fix balena install release ([243c5f4](https://gitlab.com/xom4ek/toolset/commit/243c5f49e61b542114cf8711e3cfcdfac5695a88))


### 🛠 Fixes

* **jenkins:** fix from in dockerfile release ([488ab89](https://gitlab.com/xom4ek/toolset/commit/488ab89bdeb8c7f2af9488ac2c9be3cd39aeb2b8))
* **jenkins:** fix uppercause in dockerimagename release ([63b283a](https://gitlab.com/xom4ek/toolset/commit/63b283a012421ce2f0a0612386293e553f55cc96))
* **jenkins:** remove unused env release ([a618f4b](https://gitlab.com/xom4ek/toolset/commit/a618f4b1a953276811620ec6e83fa8ebbc9082b2))

## [1.2.0](https://gitlab.com/xom4ek/toolset/compare/1.1.0...1.2.0) (2022-06-27)


### 🦊 CI/CD

* fix and run job only for tag ([2283c4d](https://gitlab.com/xom4ek/toolset/commit/2283c4d27a5a898a29c9c4a1134b131eac3f2379))


### 🚀 Features

* **ci:** add jenkinsagent image build release ([5806e46](https://gitlab.com/xom4ek/toolset/commit/5806e468fb5421a835d6213cbe40626fbd81d84f))
* **jenkinsagent:** build jenkinsagent ([a73bce1](https://gitlab.com/xom4ek/toolset/commit/a73bce1df7f95e01b49cbc8fc5335844f2f54c02))

## [1.1.0](https://gitlab.com/xom4ek/toolset/compare/1.0.0...1.1.0) (2022-06-02)


### 🦊 CI/CD

* add rebuild when release ([461b8e8](https://gitlab.com/xom4ek/toolset/commit/461b8e84883c0f3b1137a9468966b8baea85355d))
* trigger only if changes ([340323a](https://gitlab.com/xom4ek/toolset/commit/340323a30a819ac818b351a1d8e48798ae07c699))


### 🚀 Features

* **ansible:** add ansible.cfg and install community sops ([5215466](https://gitlab.com/xom4ek/toolset/commit/52154665261ed2e7b4706a4213a726185022b958))
* **ansible:** add hostchecking False release ([f3dbe47](https://gitlab.com/xom4ek/toolset/commit/f3dbe4768f5ae793680051b3c22d6d8a9b955124))

## [1.0.0](https://gitlab.com/xom4ek/toolset/compare/...1.0.0) (2022-05-26)


### ⚠ BREAKING CHANGES

* add ci

### 📔 Docs

* add readme release ([e7171c9](https://gitlab.com/xom4ek/toolset/commit/e7171c9bbe3a16cb7581a9ac82cf3f046d039dc9))


### 🦊 CI/CD

* add semantic releases ([4110165](https://gitlab.com/xom4ek/toolset/commit/411016503975f23e2324b63a38bf0192049e1abd))
* fix jobs name ([62027c9](https://gitlab.com/xom4ek/toolset/commit/62027c954c5502536b9a76404c85b99960ebb87b))
* fix ref ([c660f15](https://gitlab.com/xom4ek/toolset/commit/c660f1568380bfbb6c599643eb94f157e77a6a8b))


### 🚀 Features

* add ci ([3c28adf](https://gitlab.com/xom4ek/toolset/commit/3c28adf9605db956e29060c994d3273ac8de6655))
